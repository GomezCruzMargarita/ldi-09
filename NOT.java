/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class NOT {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        String binario1;
        Hashtable tabla  = new Hashtable();
        Scanner entrada = new Scanner(System.in);
	tabla.put("00000001","11111110");
        tabla.put("00000011","11111100");
        tabla.put("00000111","11111000");
        tabla.put("00001111","11110000");
        tabla.put("00011111","11100000");
        tabla.put("00111111","11000000");
        tabla.put("01111111","10000000");
        tabla.put("11111111","00000000");
        tabla.put("00000101","11111010");
        tabla.put("00001001","11110110");
        tabla.put("00010001","11101110");
        tabla.put("00100001","11011110");
        tabla.put("01000001","10111110");
        tabla.put("10000001","01111110");
        tabla.put("10101010","01010101");
      
       System.out.println("Operacion NOT");
       System.out.println("Ingrese el byte para aplicar operacion NOT");
       binario1=entrada.next();
       
       if(binario1.length() == 8){
       String comp1 = (String) tabla.get(binario1);
       System.out.println("Operacion NOT de "+binario1+" es: " +comp1);
       }
       else
       {
          System.err.println("El binario debe de cumplir un tamaño de 8 bits, verifica tu entrada de datos"); 
       }
    }
    
}
