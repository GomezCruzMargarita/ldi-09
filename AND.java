/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Hashtable;
import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class AND {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        String binario1;
        Hashtable tabla  = new Hashtable();
        Scanner entrada = new Scanner(System.in);
	tabla.put("11010101","11010001");
        tabla.put("11010011","11010000");
        tabla.put("00000111","00000111");
        tabla.put("00001111","00001111");
        tabla.put("00011111","00011111");
        tabla.put("10101111","10101111");
        tabla.put("01111111","01111111");
        tabla.put("11111111","00000000");
        tabla.put("00000101","00000101");
        tabla.put("00001001","00001001");
        tabla.put("10000000","10000000");
        tabla.put("00000001","00000001");
        tabla.put("11011001","11011001");
        tabla.put("10000001","10000001");
        tabla.put("10101010","10101010");
      
       System.out.println("Operacion AND");
       System.out.println("Ingrese el byte para aplicar operacion AND");
       binario1=entrada.next();
       
       if(binario1.length() == 8){
       String comp1 = (String) tabla.get(binario1);
       System.out.println("Operacion AND de "+binario1+" con 11111111 es: " +comp1);
       }
       else
       {
          System.err.println("El binario debe de cumplir un tamaño de 8 bits, verifica tu entrada de datos"); 
       }
    }
    
}
